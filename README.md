# System Scripts
This is a collection of system administration scripts I have written for my own use over the time.

Feel free to use them, without any guarantee.

## btrfs_backup
To start automatically when external drives are plugged in
90-backup.rules:
KERNEL=="sd*", ATTRS{serial}=="<HD SERIAL HERE>", TAG+="systemd", ENV{SYSTEMD_WANTS}+="backup.service"

This scripts backups your data from btrfs volumes using **btrfs send** command to one or more btrfs volumes.
Only transfers the data that has changed since the last run.
This makes it quite fast.

For that you nee
Let's call the btrfs volume you want to backup *vol_origin* and volumes where you wil store the backups *vol_dest1* and *vol_dest2*.
Remember that you can have as many destination volumes defined as you want.

What you get in _vol_dest*_ is a subvolume with an exact copy of *vol_origin* at the time the backups was run.
Remember that using brtrfs, it only takes the space for the differences between the two last backups.

This allows you to go back in time in chunks, depending on the frequency you run the script, to recover any file that has been damaged.

If you have *vol_dest* in a separate physical hard drive, you are also protected from hard disk malfunction. With the right configuration you can eve boot in read-only mode from any of these snapshots

### Configuration
Btrfs_backup is configured by modifying the script. Not very elegant, I know. I plan to move the configuration to /etc/default at some point.

The script code tries to be self explanatory. Here you have some hints.

#### What to backup and where

For the backup itself, you just need to add a list of filesystems to be backup paired with the destination filesystem to the vol_pairs array.
Here an example:

> vol_pairs["/"]="/backup_target1/root/ /backup_target2/root"  
> vol_pairs["/home"]="/backup_target1/home/ /backup_target2/home"

With this configuration, btrfs_backup will create a root filesystem copy in /backup_target1/root and in /backup_target2/root.
And a /home filesystem copy in /backup_taget1/home and in /backup_target2/home.
Inside the target directories backup2 will create a ro subvolume named with the backup date containing a copy of the full source volume.
As the subvolumes are created using snapshots and send/receive, the newly created backup only takes the space needed for the changes since the last backup.

#### Backup retention policy

This is the default retention policy. I think it's self explanatory :)

>declare retention_base=10		# Minimum number of snaps to save  
>declare retention_days=5		# Minimum number of days to save  
>declare retention_weeks=4		# Minimum number of weeks to save  
>declare retention_months=6		# Minimum number of months to save  
>declare retention_years=4		# Minimum number of years to save

### Example of use

In my own setup, I have a multidevice SSD BTRFS filesystem in raid1 configuration that I use for my working volumes.
I have separate volumes for **/** (root) and **/home**.

I have a second multidevice HD BTRFS filesystem in raid1 configuration mounted in **/backup**.

Finally I have a third miltidevice external HD BTRFS filesystem mounted in **/backup2**.

Every 30m I run btrfs_backup with a systemd timer as my first line backup.

Once a day I plug the external HD's am btrfs_backup will makes a copy also in them, as my second line backup.

When I want to recover a file, I just need to copy the file from the corresponding volume in the **/backup** directory.
If needed, I can also rely on the extrenal HDs.

If I want to revert a complete volume to the status it was at any of the backups was takes, I can use btrfs send/receive to make a full copy of it.
This can be done even for the root filesystem if I boot from a different device.

If I managed to make my root filesystem not bootable, I can give the right commant to grub and boot from one of the backup volumes to try to fix it.
